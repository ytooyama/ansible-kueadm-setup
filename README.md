# README

## はじめに

だいぶスクリプトを変えたため、Ubuntu以外のOSの対応をやめました。
他のOSでデプロイしたい場合は[branches/v1](https://gitlab.com/ytooyama/ansible-kueadm-setup/-/tree/v1)を使ってください。

## 事前準備

ノードのSWAPを無効化します(Kubeletのデフォルトではスワップパーティションがあるとクラスタ実行ができないため)。SWAPイメージもしくはSWAPパーティションをコメント化して、読み込まないように設定します。

```bash
$ sudo vi /etc/fstab
...
#/swap.img      none    swap    sw      0       0   #コメント化

$ sudo swapoff -a
```

## 動作確認した環境

### AnsibleノードのAnsibleバージョン

- Ansible 2.9以降

### デプロイ先

- Ubuntu Server 18.04.x
- Ubuntu Server 20.04.x

## Kubernetesセットアップの流れ

- hostsファイルにノードを追加
- ansible playbookのhostsのところを設定（全てで動かす場合はall）
- CRIをインストール
- kubeletなどをインストール
- クラスターを作成
- クラスターにWorkerノードを追加
- Networkアドオンを導入

## CRIのインストール

### containerd

CRIとしてcontainerdをインストールする場合は次を実行。

```bash
$ ansible-playbook ubuntu-containerd/ansible-kubeadm-containerd.yaml -i hosts
```

### CRI-O

CRIとしてCRI-Oをインストールする場合は次を実行。

```bash
$ ansible-playbook ubuntu-crio/ansible-kubeadm-crio.yaml -i hosts
```

### Docker

CRIとしてDockerをインストールする場合は次を実行。

```bash
$ ansible-playbook ubuntu-docker/ansible-kubeadm-docker.yaml -i hosts
```

## kubeletなどをインストール

Kubelet, Kubeadm, Kubectlをインストールします。バージョン指定されているので編集の上実行します。

```bash
$ ansible-playbook ansible-kubeadm-kubetools.yaml -i hosts
```

## クラスターを作成

shellの行に指定しているバージョン、`control-plane-endpoint`をMasterのIPアドレスに合わせます。 `pod-network-cidr`はCNIが想定するCIDRを指定します。Calicoの場合、デフォルトは`192.168.0.0/16`です。

```bash
$ vi ansible-kubeadm-kubeinit.yaml
---
- hosts: master
  become: yes
  tasks:
    - name: kube init
      shell: sudo kubeadm init --control-plane-endpoint=172.17.28.57 --pod-network-cidr=192.168.0.0/16 --kubernetes-version 1.20.4
```

上記編集後、実行してクラスターを作成します。

```bash
$ ansible-playbook ansible-kubeadm-kubeinit.yaml -i hosts
```

## Workerノードをクラスターに追加

Masterノードで`sudo kubeadm token create --print-join-command`コマンドを実行し、
出力されたコマンドを各Workerノードで実行する。

example:

```bash
$ sudo -i
# kubeadm join 172.17.28.57:6443 --token ygnx6t.2cgshws6n15cpozm \
    --discovery-token-ca-cert-hash sha256:1d0e465911b9c9f64f213cb29e5230d4114de2514506872bdf84db63ad25e4fd
```

### クラスターにNetwork Add-onを投入

次を実施するか...

```bash
$ ansible-playbook ansible-cni-calico.yaml -i hosts
```

もしくは次のドキュメントに従って、なんらかのCNIをセットアップする。

- [クラスターのネットワーク](https://kubernetes.io/ja/docs/concepts/cluster-administration/networking/)
- [Canalの場合](https://docs.projectcalico.org/getting-started/kubernetes/flannel/flannel)
- [Calicoの場合](https://docs.projectcalico.org/getting-started/kubernetes/quickstart)

### kubeconfigの読み込み

MasterノードにSSHアクセスしてkubeconfigを読み込みます。

```bash
$ sudo -i
# export KUBECONFIG=/etc/kubernetes/admin.conf
```

### ノードを確認

example:

```bash
# kubectl get no
NAME        STATUS   ROLES                  AGE     VERSION
esxinode7   Ready    control-plane,master   5m33s   v1.20.4
esxinode8   Ready    <none>                 3m23s   v1.20.4
esxinode9   Ready    <none>                 3m19s   v1.20.4
```
